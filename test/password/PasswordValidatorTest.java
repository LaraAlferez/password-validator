/*
 *@author Lara Alferez, 991540084  
 * This class validates testing passwords methods and it will be developed using TDD
 */
package password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testHasValidCaseChars() {
		boolean result = PasswordValidator.hasValidCaseChars("AAaaaaaAAAa");
		assertTrue("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("898989");
		assertFalse("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaa");
		assertFalse("Invalid case characters", result);
	}

	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("Aa");
		assertTrue("Invalid case characters", result);
	}

	/********                        ******/

	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("null"));
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("        "));
	}

	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}

	@Test
	public void testIsValidLengthBoundaryOut() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("123456789"));
	}

	@Test
	public void testHasRequiredDigitsRegular() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12abcdef"));
	}

	@Test
	public void testHasRequiredDigitsException() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("123bcdef"));
	}

	@Test
	public void testHasRequiredDigitsBoundaryIn() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12abcdef"));
	}

	@Test
	public void testHasRequiredDigitsBoundaryOut() {
		assertTrue("Invalid number of digits", PasswordValidator.hasReguiredDigits("12345678"));
	}

}
