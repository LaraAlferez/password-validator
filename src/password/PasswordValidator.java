/*
 *@author Lara Alferez, 991540084  
 * This class validates passwords and it will be developed using TDD
 */

package password;

public class PasswordValidator {

	private static int MIN_LENGTH = 8;

	// A password should contain uppercase & lowercase characters
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches((".*[a-z]+.*"));
	}

	// A password must have at least 8 characters
	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
		}
		return false;
	}

	// A password must have two digits
	public static boolean hasReguiredDigits(String password) {
		return password != null && password.matches("([^0-9]*[0-9]){2}.*");
	}

}
